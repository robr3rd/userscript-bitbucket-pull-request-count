// ==UserScript==
// @name         Bitbucket Pull Requext Nav Count
// @version      1.1.0
// @description  Add Pull Request count back to Bitbucket side navigation (for new nav)
// @author       Robert Robinson <robr3rd@gmail.com>
// @match https://bitbucket.org/*
// ==/UserScript==

// Count PRs in this repo + append to the "Pull Requests" nav item
(function() {
	'use strict';

	let el_nav_prs_container = document.querySelectorAll('a[href$="pull-requests/"]')[0]; // nav link to 'Pull Requests' page
	let prs_url = el_nav_prs_container.getAttribute('href');

	let xhr = new XMLHttpRequest();
	xhr.onload = function () {
		let template = document.createElement('template');
		template.innerHTML = xhr.responseText.trim();

		// Find and parse the list of PRs
		let prs_component = template.content.getElementById('pr-shared-component');
		let prs = JSON.parse(prs_component.getAttribute('data-initial-prs'));

		// Determine the text to display for the count of PRs, e.g., "3" or "25+"
		let pr_page_size = prs.page_len;
		let pr_qty = prs.values.length;
		let pr_qty_text = pr_qty;

		if (pr_qty >= pr_page_size) {
			pr_qty_text = pr_qty_text + '+';
		}

		// Create the new "PR qty text" container
		let el_pr_qty = document.createElement('span');
		el_pr_qty.textContent = ' (' + pr_qty_text + ')'; // leading space is intentional to separate from pre-existing text

		// Find the element in the "PR" nav entry that actually contains the TEXT "Pull Requests"
		let el_nav_prs = el_nav_prs_container.querySelectorAll('span:last-child > span')[0];

		// Append our new "PR count" element to the nav "Pull Requests" item
		el_nav_prs.appendChild(el_pr_qty);
	};
	xhr.open('GET', prs_url);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.send();
})();
