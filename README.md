# Bitbucket Pull Request Nav Count
This UserScript re-implements the "Pull Request counter" that could be found in the left nav on the legacy version of Bitbucket's UI.

## Installation
Just copy/paste into your favorite UserScripts browser plugin, e.g., Greasemonkey or Tampermonkey.

## Usage
Visit any repository on Bitbucket.